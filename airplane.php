<?php

include 'autoloder.php';

class Airplane extends Transport implements CanDeliver, CostOfDelivery{
    public function CanDeliver($distanceBox, $distanceTransport){
        if ($distanceTransport > $distanceBox){
            return "Да";
        }else {
            return "Нет";
        }
    }

    public function Cost($distanceBox, $transportationPrice){

        return $distanceBox * $transportationPrice ;

    }
}